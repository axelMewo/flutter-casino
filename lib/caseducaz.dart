import 'dart:math';

import 'package:flutter/material.dart';

class CaseDuCaz extends StatelessWidget {
  CaseDuCaz({
    super.key, required this.index
  });

  final int index;
  final List<String> images = [
    "bar",
    "cerise",
    "cloche",
    "diamant",
    "fer-a-cheval",
    "pasteque",
    "sept"
  ];


  @override
  Widget build(BuildContext context) {
    String symbol = images[index];
    return Image.asset('images/$symbol.png');  
  }
}