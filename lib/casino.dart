import 'dart:math';

import 'package:casino/caseducaz.dart';
import 'package:flutter/material.dart';

class Casino extends StatefulWidget {
  const Casino({super.key, required this.title});
  final String title;

  @override
  State<Casino> createState() => _CasinoState();
}

class _CasinoState extends State<Casino> {
  final rand = Random();
  late int _index1;
  late int _index2;
  late int _index3;

  bool _isJackpot = false;

  @override
  void initState() {
    _index1 = rand.nextInt(7);
    _index2 = rand.nextInt(7);
    _index3 = rand.nextInt(7);

    super.initState();
  }

  void _onPressed() {
    var rnd1 = rand.nextInt(7);
    var rnd2 = rand.nextInt(7);
    var rnd3 = rand.nextInt(7);

    setState(() {
      _isJackpot = rnd1 == rnd2 && rnd2 == rnd3;

      _index1 = rnd1;
      _index2 = rnd2;
      _index3 = rnd3;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Center(
          child: Text(
            widget.title,
            textAlign: TextAlign.center,
          ),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 100,
                  height: 100,
                  child: CaseDuCaz(index: _index1),
                ),

                const SizedBox(width: 20), // Espace entre les widgets CaseDuCaz
                SizedBox(
                  width: 100,
                  height: 100,
                  child: CaseDuCaz(index: _index2),
                ),

                const SizedBox(width: 20), // Espace entre les widgets CaseDuCaz
                SizedBox(
                  width: 100,
                  height: 100,
                  child: CaseDuCaz(index: _index3),
                ),
              ],
            ),

            const SizedBox(height: 80), // separateur

            Text(
              _isJackpot ? 'JACKPOT = true !' : '',
              style: const TextStyle(fontSize: 50),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.blue,
          onPressed: _onPressed,
          child: const Icon(Icons.add)),
    );
  }
}
